package com.itpaths.artesia.dam.model;

public class Node {
    private Node parent;
    private String name;
    private int index;
    private String key;
    private String path;

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Node{" +
                "\nparent=" + parent +
                ",\nname='" + name + '\'' +
                ",\nindex=" + index +
                ",\nkey='" + key + '\'' +
                ",\npath='" + path + '\'' + '\n' +
                '}';
    }
}